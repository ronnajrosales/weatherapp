package app.takehomeexam.weatherapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ronalyn on 25/08/2018.
 */

public class WeatherGroup {
    @SerializedName("cnt")
    @Expose
    private Integer cnt;
    @SerializedName("list")
    @Expose
    private java.util.List<CompleteWeather> list = null;

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public java.util.List<CompleteWeather> getList() {
        return list;
    }

    public void setList(List<CompleteWeather> list) {
        this.list = list;
    }
}