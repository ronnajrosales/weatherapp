package app.takehomeexam.weatherapp.di.modules;


import app.takehomeexam.weatherapp.MainActivity;
import app.takehomeexam.weatherapp.ui.weatherdetails.WeatherDetailsActivity;
import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ronalyn on 25/08/2018.
 */

@Module(includes = AndroidInjectionModule.class)
public abstract class ActivityBindingsModule {

    @PerActivity
    @ContributesAndroidInjector(modules = {MainActivityBuilderModule.class, MainActivityModule.class})
    abstract MainActivity mainActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = {WeatherDetailsActivityModule.class})
    abstract WeatherDetailsActivity weatherDetailsActivityInjector();

}