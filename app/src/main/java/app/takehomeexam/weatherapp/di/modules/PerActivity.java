package app.takehomeexam.weatherapp.di.modules;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by ronalyn on 25/08/2018.
 */

@Scope
@Retention(RUNTIME)
public @interface PerActivity {}
