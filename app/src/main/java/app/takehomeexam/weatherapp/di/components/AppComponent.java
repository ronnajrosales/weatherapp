package app.takehomeexam.weatherapp.di.components;

import android.app.Application;

import javax.inject.Singleton;

import app.takehomeexam.weatherapp.WeatherApplication;
import app.takehomeexam.weatherapp.di.modules.ActivityBindingsModule;
import app.takehomeexam.weatherapp.di.modules.AppModule;
import app.takehomeexam.weatherapp.di.modules.DatabaseModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by ronalyn on 25/08/2018.
 */

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, DatabaseModule.class, AppModule.class, ActivityBindingsModule.class})
public interface AppComponent extends AndroidInjector<WeatherApplication> {

    void inject(WeatherApplication instance);

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application daggerApplication);

        AppComponent build();
    }
}