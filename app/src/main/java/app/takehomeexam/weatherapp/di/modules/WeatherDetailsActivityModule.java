package app.takehomeexam.weatherapp.di.modules;


import app.takehomeexam.weatherapp.ui.weatherdetails.WeatherDetailsContract;
import app.takehomeexam.weatherapp.ui.weatherdetails.WeatherDetailsPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ronalyn on 25/08/2018.
 */
@Module
public class WeatherDetailsActivityModule {

    @Provides
    WeatherDetailsContract.Presenter  providesWeatherDetailsActivity (WeatherDetailsPresenter weatherDetailsPresenter) {
        return weatherDetailsPresenter;
    }



}