package app.takehomeexam.weatherapp.di.modules;

import app.takehomeexam.weatherapp.ui.refresh.RefreshButtonContract;
import app.takehomeexam.weatherapp.ui.refresh.RefreshButtonPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ronalyn on 25/08/2018.
 */
@Module
public class RefreshFragmentModule {

    @Provides
    RefreshButtonContract.Presenter  providesPresenter (RefreshButtonPresenter refreshButtonPresenter) {
        return refreshButtonPresenter;
    }

}
