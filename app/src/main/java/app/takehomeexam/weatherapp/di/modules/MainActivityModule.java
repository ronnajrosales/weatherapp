package app.takehomeexam.weatherapp.di.modules;

import app.takehomeexam.weatherapp.usecase.LocationUseCaseImpl;
import app.takehomeexam.weatherapp.usecase.contracts.LocationUseCase;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ronalyn on 25/08/2018.
 */
@Module
public class MainActivityModule {

    @Provides
    @PerActivity
    LocationUseCase getLocationUseCase(LocationUseCaseImpl locationUseCase) {
        return locationUseCase;
    }
}
