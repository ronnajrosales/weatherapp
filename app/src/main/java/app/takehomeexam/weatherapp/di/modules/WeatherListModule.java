package app.takehomeexam.weatherapp.di.modules;

import app.takehomeexam.weatherapp.ui.weatherlist.WeatherListContract;
import app.takehomeexam.weatherapp.ui.weatherlist.WeatherListPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ronalyn on 25/08/2018.
 */

@Module
public class WeatherListModule {

    @Provides
    WeatherListContract.Presenter providesWeatherListPresenter(WeatherListPresenter presenter) {
        return presenter;
    }
}
