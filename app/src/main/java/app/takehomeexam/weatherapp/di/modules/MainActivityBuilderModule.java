package app.takehomeexam.weatherapp.di.modules;


import app.takehomeexam.weatherapp.ui.refresh.RefreshButtonFragment;
import app.takehomeexam.weatherapp.ui.weatherlist.WeatherListFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ronalyn on 25/08/2018.
 */
@Module
public abstract  class MainActivityBuilderModule {


    @PerFragment
    @ContributesAndroidInjector(modules = RefreshFragmentModule.class)
    abstract RefreshButtonFragment bindRefreshButtonFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = WeatherListModule.class)
    abstract WeatherListFragment bindWeatherListFragment();

}