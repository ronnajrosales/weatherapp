package app.takehomeexam.weatherapp.ui.weatherdetails;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import app.takehomeexam.weatherapp.networkcalls.APIClient;
import app.takehomeexam.weatherapp.networkcalls.APIInterface;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ronalyn on 26/08/2018.
 */

public class WeatherDetailsPresenter implements WeatherDetailsContract.Presenter{

    private WeatherDetailsContract.View mView;
    private String mCityId;
    private APIInterface apiInterface;

    @Inject
    public WeatherDetailsPresenter() {

    }


    @Override
    public void setCityId(String cityId) {
        mCityId = cityId;
    }

    @Override
    public void setView(WeatherDetailsContract.View view) {
        mView = view;
    }

    @Override
    public void start() {
        mView.showProgress(true);
        mView.showRetry(false);
        mView.showWeatherDetails(false);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        getWeatherDetails();

    }

    @Override
    public void getWeatherDetails() {
        apiInterface.getCompleteWeatherByCountryIds(getParameters(String.valueOf(mCityId))).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(completeWeather -> {
                    mView.updateWeatherDetails(completeWeather);
                    mView.showWeatherDetails(true);
                    mView.showProgress(false);
                }, throwable -> {
                    throwable.printStackTrace();
                    mView.showRetry(true);
                    mView.showProgress(false);
                });
    }

    private Map<String, String> getParameters(String id) {
        Map<String, String> data = new HashMap<>();
        data.put("id", id);
        data.put("appid", "b6907d289e10d714a6e88b30761fae22");
        data.put("units", "metric");
        return data;
    }

}