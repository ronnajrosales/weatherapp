package app.takehomeexam.weatherapp.ui.weatherlist;

import java.util.List;

import app.takehomeexam.weatherapp.model.CompleteWeather;

/**
 * Created by ronalyn on 25/08/2018.
 */

public interface WeatherListContract {
    interface View {

        void displayWeatherList(List<CompleteWeather> completeWeatherList);
        void addCurrentLocationWeather(CompleteWeather completeWeatherList);

        void showProgressBar(boolean show);

        void showErrorMessage(boolean show);

        void showWeatherList(boolean show);

        void clearList();

    }

    interface Presenter  {

        void setView(WeatherListContract.View view);

        void start();

        void destroy();
    }

    interface OnWeatherClick {

        void onWeatherSelected(String cityId);



    }

}
