package app.takehomeexam.weatherapp.ui.weatherdetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import app.takehomeexam.weatherapp.R;
import app.takehomeexam.weatherapp.model.Clouds;
import app.takehomeexam.weatherapp.model.CompleteWeather;
import app.takehomeexam.weatherapp.model.Coord;
import app.takehomeexam.weatherapp.model.Main;
import app.takehomeexam.weatherapp.model.Weather;
import app.takehomeexam.weatherapp.model.Wind;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by ronalyn on 25/08/2018.
 */

public class WeatherDetailsActivity extends DaggerAppCompatActivity implements WeatherDetailsContract.View {

    @BindView(R.id.tv_city_name)
    TextView tvCityName;

    @BindView(R.id.iv_icon)
    ImageView ivWeatherIcon;

    @BindView(R.id.tv_temperature)
    TextView tvTemperature;

    @BindView(R.id.tv_coor_lan)
    TextView tvCoorLan;

    @BindView(R.id.tv_coor_long)
    TextView tvCoorLong;

    @BindView(R.id.tv_pressure)
    TextView tvPressure;

    @BindView(R.id.tv_humidity)
    TextView tvHumidity;

    @BindView(R.id.tv_min_temp)
    TextView tvMinTemp;

    @BindView(R.id.tv_max_temp)
    TextView tvMaxTemp;

    @BindView(R.id.tv_wind_speed)
    TextView tvWindSpeed;

    @BindView(R.id.tv_wind_deg)
    TextView tvWindDeg;

    @BindView(R.id.tv_clouds)
    TextView tvClouds;

    @BindView(R.id.progress_bar_container)
    LinearLayout llProgressContainer;

    @BindView(R.id.details_card_1)
    CardView cardViewHeader;

    @BindView(R.id.details_card_2)
    CardView cardViewDetails;

    @BindView(R.id.refresh_button)
    Button btnRefresh;

    @Inject
    WeatherDetailsContract.Presenter presenter;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);
        ButterKnife.bind(this);

        presenter.setView(this);
        parseArguments();
        presenter.start();

    }

    private void parseArguments() {
        if (getIntent().hasExtra("cityId")) {
            presenter.setCityId(getIntent().getStringExtra("cityId"));
        }
    }

    @Override
    public void updateWeatherDetails(CompleteWeather completeWeather) {
        tvCityName.setText(completeWeather.getName());

        updateMain(completeWeather.getMain());
        updateWind(completeWeather.getWind());
        updateCloud(completeWeather.getClouds());
        updateCoordinates(completeWeather.getCoord());
        updateWeather(completeWeather.getWeather().get(0));
    }

    @Override
    public void showProgress(boolean show) {
        llProgressContainer.setVisibility(show ? View.VISIBLE : View.GONE);

    }

    @Override
    public void showRetry(boolean show) {
        btnRefresh.setVisibility(show ?  View.VISIBLE : View.GONE);
    }

    @Override
    public void showWeatherDetails(boolean show) {
        cardViewHeader.setVisibility(show ? View.VISIBLE : View.GONE);
        cardViewDetails.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void updateCloud(Clouds clouds) {
        tvClouds.setText(getString(R.string.cloud_percentage).concat(" ").concat(String.valueOf(clouds.getAll())));
    }

    private void updateWind(Wind wind) {
        tvWindDeg.setText(getString(R.string.wind_deg).concat(" ").concat(String.valueOf(wind.getDeg())));
        tvWindSpeed.setText(getString(R.string.wind_speed).concat(String.valueOf(wind.getSpeed())));
    }

    private void updateMain(Main main) {
        tvTemperature.setText(String.valueOf(main.getTemp()).concat("˚C"));
        tvPressure.setText(getString(R.string.pressure).concat(" ").concat(String.valueOf(main.getPressure())));
        tvHumidity.setText(getString(R.string.humidity).concat(" ").concat(String.valueOf(main.getHumidity())));
        tvMinTemp.setText(getString(R.string.min_temp).concat(" ").concat(String.valueOf(main.getTempMin()).concat("˚C")));
        tvMaxTemp.setText(getString(R.string.max_temp).concat(" ").concat(String.valueOf(main.getTempMax()).concat("˚C")));
    }

    private void updateWeather(Weather weather) {
        String url = "http://openweathermap.org/img/w/".concat(weather.getIcon()).concat(".png");
        Picasso.with(this).load(url).into(ivWeatherIcon);
    }

    private void updateCoordinates(Coord coord) {
        tvCoorLong.setText(getString(R.string.long_label).concat(" ").concat(String.valueOf(coord.getLon())));
        tvCoorLan.setText(getString(R.string.lan_label).concat(" ").concat(String.valueOf(coord.getLon())));
    }

    @OnClick(R.id.refresh_button)
    public void onClickRefresh() {
        showRetry(false);
        presenter.getWeatherDetails();
    }
}
