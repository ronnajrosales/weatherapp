package app.takehomeexam.weatherapp.ui.weatherlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import app.takehomeexam.weatherapp.R;
import app.takehomeexam.weatherapp.ui.weatherdetails.WeatherDetailsActivity;
import app.takehomeexam.weatherapp.model.CompleteWeather;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

/**
 * Created by ronalyn on 25/08/2018.
 */

public class WeatherListFragment extends DaggerFragment implements WeatherListContract.View, WeatherListContract.OnWeatherClick {

    @BindView(R.id.weather_list)
    RecyclerView weatherRecyclerView;

    @BindView(R.id.progress_bar_container)
    LinearLayout llProgressContainer;

    @BindView(R.id.error_text)
    TextView tvError;

    @Inject
    WeatherListContract.Presenter presenter;

    WeatherAdapter weatherAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_weather_list, container, false);
        ButterKnife.bind(this, root);

        presenter.setView(this);
        presenter.start();

        initializeAdapter();

        return root;
    }

    private void initializeAdapter() {
        LinearLayoutManager layoutManager= new LinearLayoutManager(getContext());
        //when you want vertical
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        weatherRecyclerView.setLayoutManager(layoutManager);
        weatherAdapter = new WeatherAdapter(getActivity(), this);
        weatherRecyclerView.setAdapter(weatherAdapter);
    }


    @Override
    public void displayWeatherList(List<CompleteWeather> completeWeatherList) {
        weatherAdapter.setWeatherData(completeWeatherList);
        showWeatherList(true);
    }

    @Override
    public void addCurrentLocationWeather(CompleteWeather completeWeatherList) {
        weatherAdapter.addCurrentLocationWeatherData(completeWeatherList);
        showWeatherList(true);
    }

    @Override
    public void onWeatherSelected(String cityId) {
        Intent intent = new Intent(getActivity(), WeatherDetailsActivity.class);
        intent.putExtra("cityId", cityId);
        startActivity(intent);
    }

    @Override
    public void showProgressBar(boolean show) {
        llProgressContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showErrorMessage(boolean show) {
        tvError.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showWeatherList(boolean show) {
        weatherRecyclerView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void clearList() {
        weatherAdapter.clearWeather();
    }
}
