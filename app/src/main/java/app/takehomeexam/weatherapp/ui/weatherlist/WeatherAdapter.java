package app.takehomeexam.weatherapp.ui.weatherlist;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.takehomeexam.weatherapp.R;
import app.takehomeexam.weatherapp.model.CompleteWeather;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ronalyn on 20/06/2018.
 */

public class WeatherAdapter extends RecyclerView.Adapter {

    public WeatherListContract.OnWeatherClick mOnClickListener;
    public Context mContext;
    private List<CompleteWeather> weatherList = new ArrayList<>();


    public WeatherAdapter(Context context, WeatherListContract.OnWeatherClick onWeatherClick) {
        mContext = context;
        mOnClickListener = onWeatherClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.weather_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.populateView(weatherList.get(position));
    }



    @Override
    public int getItemCount() {

        return weatherList.size();
    }

    public void setWeatherData(List<CompleteWeather> list) {
        this.weatherList.addAll(list);
        notifyDataSetChanged();
    }

    public void clearWeather() {
        weatherList.clear();
    }

    public void addCurrentLocationWeatherData(CompleteWeather completeWeather) {
        this.weatherList.add(completeWeather);
        notifyDataSetChanged();
    }

     class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_city_name)
        TextView tvCityName;

        @BindView(R.id.tv_temperature)
        TextView tvTemperature;

        @BindView(R.id.tv_actual_weather)
        TextView tvActualWeather;

        @BindView(R.id.iv_icon)
        ImageView ivIcon;

        @BindView(R.id.weather_cardview)
        CardView weatherCardview;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void populateView(CompleteWeather completeWeather) {
            tvCityName.setText(completeWeather.getName());
            tvTemperature.setText(String.valueOf(Math.round(completeWeather.getMain().getTemp())).concat( "˚C"));
            tvActualWeather.setText(String.valueOf(completeWeather.getWeather().get(0).getMain()));
            String url = "http://openweathermap.org/img/w/".concat(completeWeather.getWeather()
                    .get(0).getIcon()).concat(".png");
            Picasso.with(mContext).load(url).into(ivIcon);


            weatherCardview.setOnClickListener(view -> {
                mOnClickListener.onWeatherSelected(String.valueOf(completeWeather.getId()));
            });
        }
    }
}

