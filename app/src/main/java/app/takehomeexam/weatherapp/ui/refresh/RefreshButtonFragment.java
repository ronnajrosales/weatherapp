package app.takehomeexam.weatherapp.ui.refresh;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import app.takehomeexam.weatherapp.R;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerFragment;

/**
 * Created by ronalyn on 25/08/2018.
 */

public class RefreshButtonFragment extends DaggerFragment implements RefreshButtonContract.View{

    @Inject
    RefreshButtonContract.Presenter presenter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_refresh_button, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @OnClick(R.id.refresh_button)
    public void onRefreshButtonClick() {
        presenter.refreshWeatherData();
    }


}
