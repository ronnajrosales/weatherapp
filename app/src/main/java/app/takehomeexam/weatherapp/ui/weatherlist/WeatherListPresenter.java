package app.takehomeexam.weatherapp.ui.weatherlist;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import app.takehomeexam.weatherapp.model.CompleteWeather;
import app.takehomeexam.weatherapp.networkcalls.APIClient;
import app.takehomeexam.weatherapp.networkcalls.APIInterface;
import app.takehomeexam.weatherapp.usecase.contracts.LocationUseCase;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ronalyn on 25/08/2018.
 */

public class WeatherListPresenter implements WeatherListContract.Presenter {

    private LocationUseCase mLocationUseCase;
    private APIInterface apiInterface;
    private WeatherListContract.View mView;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public WeatherListPresenter(LocationUseCase locationUseCase) {
        mLocationUseCase = locationUseCase;
    }

    @Override
    public void setView(WeatherListContract.View view) {
        mView = view;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    @Override
    public void start() {
        mView.showProgressBar(true);
        getWeatherData();
        initLocationListenerObservable();
        initRefreshDataListenerObservable();
    }

    @Override
    public void destroy() {
        compositeDisposable.clear();
    }

    private void initRefreshDataListenerObservable() {
        compositeDisposable.add(mLocationUseCase.refreshDataListener()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(refresh -> {
                    mView.showProgressBar(true);
                    mView.showWeatherList(false);
                    mView.showErrorMessage(false);
                    getWeatherData();
                }));
    }

    private void initLocationListenerObservable() {
        compositeDisposable.add(mLocationUseCase.locationChangeListener()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locationMap -> {
                    mView.showWeatherList(false);
                    getCurrentWeatherData(locationMap);
                }));
    }

    private void getWeatherData() {

        apiInterface = APIClient.getClient().create(APIInterface.class);
        List<Long> ids = new ArrayList<>();
        ids.add(5391959L);
        ids.add(5367815L);
        ids.add(4548393L);

        compositeDisposable.add(Observable.fromIterable(ids)
                .flatMap(id -> apiInterface.getCompleteWeatherByCountryIds(getParameters(String.valueOf(id), "", "")))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toList()
                .subscribe(completeWeatherList -> {
                    for (CompleteWeather completeWeather : completeWeatherList) {
                        Log.d("weatherDebug", "name:" + completeWeather.getName());
                        Log.d("weatherDebug", "temp:" + completeWeather.getMain().getTemp());
                    }
                    mView.displayWeatherList(completeWeatherList);
                    mView.showProgressBar(false);
                }, throwable -> {
                    throwable.printStackTrace();
                    mView.showProgressBar(false);
                    mView.showErrorMessage(true);
                }));

    }

    private void getCurrentWeatherData(Map<String, String> locationMap) {
        String lat = locationMap.get("lat");
        String lon = locationMap.get("lon");
        compositeDisposable.add(apiInterface.getCompleteWeatherByCountryIds(getParameters("", lat, lon))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(completeWeather -> {
                    mView.addCurrentLocationWeather(completeWeather);
                    mView.showProgressBar(false);
                }, throwable -> {
                    throwable.printStackTrace();
                    mView.showProgressBar(false);
                    mView.showErrorMessage(true);
                }));
    }



    private Map<String, String> getParameters(String id, String lat, String lon) {
        Map<String, String> data = new HashMap<>();
        data.put("appid", "b6907d289e10d714a6e88b30761fae22");
        data.put("units", "metric");

        if (!lat.isEmpty()) {
            data.put("lat", lat);
            data.put("lon", lon);
        } else {
            data.put("id", id);
        }
        return data;
    }
}
