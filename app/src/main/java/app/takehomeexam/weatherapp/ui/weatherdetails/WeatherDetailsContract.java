package app.takehomeexam.weatherapp.ui.weatherdetails;

import app.takehomeexam.weatherapp.model.CompleteWeather;

/**
 * Created by ronalyn on 26/08/2018.
 */

public interface WeatherDetailsContract  {

    interface View {
        void updateWeatherDetails(CompleteWeather completeWeather);

        void showProgress(boolean show);

        void showRetry(boolean show);

        void showWeatherDetails(boolean show);

    }

    interface Presenter  {

        void getWeatherDetails();

        void setCityId(String cityId);

        void setView(WeatherDetailsContract.View view);

        void start();
    }

}
