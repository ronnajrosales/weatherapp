package app.takehomeexam.weatherapp.ui.refresh;

/**
 * Created by ronalyn on 26/08/2018.
 */

public interface RefreshButtonContract {

    interface View {

    }

    interface Presenter  {

        void setView(RefreshButtonContract.View view);

        void start();

        void refreshWeatherData();
    }

}
