package app.takehomeexam.weatherapp.ui.refresh;

import javax.inject.Inject;

import app.takehomeexam.weatherapp.usecase.contracts.LocationUseCase;

/**
 * Created by ronalyn on 26/08/2018.
 */

public class RefreshButtonPresenter implements RefreshButtonContract.Presenter {

    private LocationUseCase mLocationUseCase;
    private RefreshButtonContract.View mView;

    @Inject
    public RefreshButtonPresenter(LocationUseCase locationUseCase) {
        mLocationUseCase = locationUseCase;

    }

    @Override
    public void setView(RefreshButtonContract.View view) {
        mView = view;
    }

    @Override
    public void start() {

    }

    @Override
    public void refreshWeatherData() {
        mLocationUseCase.onRefreshData();
    }
}
