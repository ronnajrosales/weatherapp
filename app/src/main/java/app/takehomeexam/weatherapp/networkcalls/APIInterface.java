package app.takehomeexam.weatherapp.networkcalls;

import java.util.Map;

import app.takehomeexam.weatherapp.model.CompleteWeather;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by RONALYN on 8/25/2018.
 */

public interface APIInterface {

    @GET("weather?")
    Observable<CompleteWeather> getCompleteWeatherByCountryIds(@QueryMap Map<String, String> options);

}
