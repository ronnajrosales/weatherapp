package app.takehomeexam.weatherapp.usecase.contracts;

import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by ronalyn on 26/08/2018.
 */

public interface LocationUseCase {

    Observable<Map<String, String>> locationChangeListener();

    void broadcastLocation(Double lat, Double lon);

    void onRefreshData();

    Observable<Boolean> refreshDataListener();

}
