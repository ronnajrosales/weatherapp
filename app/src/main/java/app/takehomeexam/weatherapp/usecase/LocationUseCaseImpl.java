package app.takehomeexam.weatherapp.usecase;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import app.takehomeexam.weatherapp.di.modules.PerActivity;
import app.takehomeexam.weatherapp.usecase.contracts.LocationUseCase;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by ronalyn on 26/08/2018.
 */

@PerActivity
public class LocationUseCaseImpl implements LocationUseCase {

    PublishSubject<Map<String, String>> locationChangeSubject = PublishSubject.create();
    PublishSubject<Boolean> refreshDataObservable = PublishSubject.create();

    @Inject
    public LocationUseCaseImpl() {

    }

    @Override
    public Observable<Map<String, String>> locationChangeListener() {
        return locationChangeSubject.hide();
    }

    @Override
    public void broadcastLocation(Double lat, Double lon) {
        Map<String, String> locationMap = new HashMap<>();
        locationMap.put("lon", String.valueOf(lon));
        locationMap.put("lat", String.valueOf(lat));

        locationChangeSubject.onNext(locationMap);

    }

    @Override
    public void onRefreshData() {
        refreshDataObservable.onNext(true);
    }

    @Override
    public Observable<Boolean> refreshDataListener() {
        return refreshDataObservable.hide();
    }


}
